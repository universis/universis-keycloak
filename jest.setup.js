const { JsonLogger } = require('@themost/json-logger');
const {TraceUtils} = require('@themost/common');
require('dotenv').config();
TraceUtils.useLogger(new JsonLogger({
    format: 'raw'
}));

jest.setTimeout(120000);
