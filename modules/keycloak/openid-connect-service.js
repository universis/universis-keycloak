const {ApplicationService, TraceUtils} = require('@themost/common');
const { Request } = require('superagent');
const {Observable} = require('rxjs');
const {shareReplay} = require('rxjs/operators');
const { responseHandler } = require('./response-callback');
const {URL} = require('url');


class OpenIDConnectService extends ApplicationService {
    constructor(app) {
        super(app);
        // set settings property
        const configuration = app.getConfiguration();
        this.settings = configuration.getSourceAt("settings/auth") || {};

        if (this.settings.issuer_uri == null) {
            TraceUtils.warn('KeycloakClientService', 'Issuer URI is not defined or is inaccessible. The operation will try to extract issuer URI from server URI.');
            if (this.settings.server_uri == null) {
                TraceUtils.warn('KeycloakClientService', 'An application configuration setting is missing. Server URI is not defined or is inaccessible.');
            }
            // try to validate server_uri
            // keycloak server uri should end with /protocol/openid-connect
            // so, try to search and replace this segment for resolving issuer_uri, and it's being defined
            // at well known configuration service
            const serverURL = new URL(this.settings.server_uri);
            const re = /protocol\/openid-connect\/?$/ig;
            // search if server_uri ends with /protocol/openid-connect
            if (re.test(serverURL.pathname)) {
                // replace string
                serverURL.pathname = serverURL.pathname.replace(re, '');
                // and set issuer_uri
                this.settings.issuer_uri = serverURL.toString();
                TraceUtils.info('KeycloakClientService', `Issuer URI has been set to ${this.settings.issuer_uri}`);
            } else {
                TraceUtils.warn('KeycloakClientService', `It seems that server URI does not end with /protocol/openid-connect. The service will use ${this.settings.issuer_uri} as issuer_uri.`);
                this.settings.issuer_uri = this.settings.server_uri;
            }
            configuration.setSourceAt('settings/auth/issuer_uri', this.settings.issuer_uri);
        }
        if (this.settings.admin_uri == null) {
            TraceUtils.warn('KeycloakClientService', 'Admin URI is not defined or is inaccessible. The operation will try to extract admin URI from issuer URI.');
            const re = /(realms\/(\w+)\/?$)/ig;
            this.settings.admin_uri = this.settings.issuer_uri.replace(re, 'admin/$1');
            TraceUtils.info('KeycloakClientService', `Admin URI has been set to ${this.settings.admin_uri}`);
            configuration.setSourceAt('settings/auth/admin_uri', this.settings.admin_uri);
        }

        // noinspection JSValidateTypes
        /**
         * @type {Observable<OpenIDConfiguration>}
         */
        this.wellKnownConfiguration = new Observable((observer) => {
            void new Promise((resolve, reject) => {
                const issuer = this.settings.issuer_uri;
                if (issuer == null) {
                    return reject(new Error('An application configuration setting is missing. Issuer URI is not defined or is inaccessible.'));
                }
                return new Request('GET',new URL('.well-known/openid-configuration', issuer))
                    .set('Accept', 'application/json')
                    .end(responseHandler(resolve, reject));
            }).then((result) => {
                observer.next(result);
                observer.complete();
            }).catch((err) => {
                observer.error(err);
            });
        }).pipe(
            shareReplay(1)
        );
    }
}

module.exports = {
    OpenIDConnectService
}
