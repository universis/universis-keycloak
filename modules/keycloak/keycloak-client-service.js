const { DataError } = require('@themost/common');
const {URL} = require('url');
const { Request } = require('superagent');
const { responseHandler } = require('./response-callback');
const { OpenIDConnectService } = require('./openid-connect-service');
const {KeycloakTokenIntrospectionStrategy} = require('./keycloak-introspection-strategy');
const {HttpTokenIntrospectionStrategy} = require('./http-token-introspection-strategy');
const introspectionService = Symbol('introspectionService');
/**
 * @typedef {object} KeycloakUserProfile
 * @property {string} sub (e.g. 00000000-0000-0000-0000-432730e4f6b2)
 * @property {string} name
 * @property {string} preferred_username
 * @property {string} given_name
 * @property {string} family_name
 * @property {string} email
 */

/**
 * @typedef {object} KeycloakAuthorizeUser
 * @property {string} client_id
 * @property {string=} client_secret
 * @property {string} username
 * @property {string} password
 * @property {string} grant_type
 * @property {string=} scope
 */

/**
 * @typedef {object} KeycloakMethodOptions
 * @property {string} access_token
 */


/**
 * @class
 * @property {OpenIDConnectConfiguration} settings
 * @property {OpenIDConfiguration} wellKnownConfiguration
 */
class KeycloakClientService extends OpenIDConnectService {
    /**
     * @param {import('@themost/common').ApplicationBase} app
     */
    constructor(app) {
        super(app);
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets user profile by calling keycloak server userinfo endpoint
     * @param {*} context
     * @param {string} token
     * @returns {Promise<KeycloakUserProfile>}
     */
    getProfile(context, token) {
        if (token == null) {
            return Promise.reject(new Error('User token cannot be empty at this context.'));
        }
        return new Promise((resolve, reject)=> {
            void this.wellKnownConfiguration.subscribe((wellKnownConfiguration) => {
                const { userinfo_endpoint } = wellKnownConfiguration;
                if (userinfo_endpoint == null) {
                    return reject(new Error('Userinfo endpoint is not defined or is inaccessible.'));
                }
                new Request('GET', userinfo_endpoint).set({
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json'
                }).end(responseHandler(resolve, reject));
            }, (err) => {
                return reject(err);
            });
        });
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets user profile by calling keycloak server userinfo endpoint
     * @param {*} context
     * @param {string} token
     * @returns {Promise<KeycloakUserProfile>}
     */
    getUserInfo(context, token) {
        return this.getProfile(context, token);
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets the token info of the current context
     * @param {*} context
     */
    getContextTokenInfo(context) {
        if (context.user == null) {
            return Promise.reject(new Error('Context user may not be null'));
        }
        if (context.user.authenticationType !== 'Bearer') {
            return Promise.reject(new Error('Invalid context authentication type'));
        }
        if (context.user.authenticationToken == null) {
            return Promise.reject(new Error('Context authentication data may not be null'));
        }
        return this.getTokenInfo(context, context.user.authenticationToken);
    }

    /**
     * Gets token info by calling keycloak server endpoint
     * @param {*} context
     * @param {string} token
     */
    getTokenInfo(context, token) {
        const introspection = this.application.getService(KeycloakTokenIntrospectionStrategy);
        if (introspection == null) {
            // use fallback introspection service
            if (this[introspectionService] == null) {
                this[introspectionService] = new HttpTokenIntrospectionStrategy(this.application);
            }
            return this[introspectionService].introspect(token);
        }
        return introspection.introspect(token);
    }

    /**
     * @param {KeycloakAuthorizeUser} authorizeUser
     */
    authorize(authorizeUser) {
        return new Promise((resolve, reject)=> {
            void this.wellKnownConfiguration.subscribe((wellKnownConfiguration) => {
                const { token_endpoint } = wellKnownConfiguration;
                if (token_endpoint == null) {
                    return reject(new Error('Token endpoint is not defined or is inaccessible.'));
                }
                new Request('POST', token_endpoint)
                    .type('form')
                    .send(authorizeUser)
                    .end(responseHandler(resolve, reject));
            }, (err) => {
                return reject(err);
            });
        });
    }

    /**
     * Gets a user by name
     * @param {*} user_id 
     * @param {KeycloakMethodOptions} options 
     */
     getKeycloakUserById(user_id, options) {
        return new Promise((resolve, reject) => {
            void new Request('GET', new URL(`users/${user_id}`, this.settings.admin_uri))
                .set('Authorization', `Bearer ${options.access_token}`)
                .end(responseHandler(resolve, reject));
        });
    }

    /**
     * Gets a user by name
     * @param {*} user_id 
     * @param {KeycloakMethodOptions} options 
     */
     async getUserById(user_id, options) {
        const result = await this.getKeycloakUserById(user_id, options);
        if (result) {
            return this.convertTo(result);
        }
    }

    /**
     * Gets a user by name
     * @param {string} username 
     * @param {KeycloakMethodOptions} options 
     */
     getKeycloakUser(username, options) {
        return new Promise((resolve, reject)=> {
            void new Request('GET', new URL(`users`, this.settings.admin_uri))
                .set('Authorization', `Bearer ${options.access_token}`)
                .query({
                    username: username
                })
                .end(responseHandler(resolve, reject));
        }).then((results) => {
            return results.find((item) => {
                return item.username === username;
            });
        });
    }

    /**
     * Gets a user by name
     * @param {string} username 
     * @param {KeycloakMethodOptions} options 
     */
    async getUser(username, options) {
        const result = await this.getKeycloakUser(username, options);
        if (result) {
            return this.convertTo(result);
        }
    }

    /**
     * Gets a user by email address
     * @param {string} email 
     * @param {KeycloakMethodOptions} options 
     */
     getKeycloakUserByEmail(email, options) {
        return new Promise((resolve, reject)=> {
            void new Request('GET', new URL(`users`, this.settings.admin_uri))
                .set('Authorization', `Bearer ${options.access_token}`)
                .query({
                    email: email
                })
                .end(responseHandler(resolve, reject));
        }).then((results) => {
            return results.find((item) => {
                return item.email === email;
            });
        });
    }

    /**
     * Gets a user by email address
     * @param {string} email 
     * @param {KeycloakMethodOptions} options 
     */
     async getUserByEmail(email, options) {
        const result = await this.getKeycloakUserByEmail(email, options);
        if (result) {
            return this.convertTo(result);
        }
    }

    /**
     * Updates an existing user
     * @param {GenericUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     async updateUser(user, options) {
        // convert generic user
        const updateUser = this.convertFrom(user);
        // create keycloak user
        await this.updateKeycloakUser(updateUser, options);
        // finally convert new user to generic user
        return user;
    }

    /**
     * Updates an existing user
     * @param {KeycloakUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     updateKeycloakUser(user, options) {
        return new Promise((resolve, reject)=> {
            if (user.id == null) {
                return reject(new DataError('E_IDENTIFIER', 'User may not be empty at this context.', null, 'User', 'id'));
            }
            new Request('PUT', new URL(`users/${user.id}`, this.settings.admin_uri))
                .set('Authorization', `Bearer ${options.access_token}`)
                .send(user)
                .end(responseHandler(resolve, reject));
        });
    }

    /**
     * Converts a keycloak user to a generic user
     * @param {KeycloakUser|*} user
     */
    convertTo(user) {
        if (user) {
            return {
                id: user.id,
                name: user.username,
                alternateName: user.email,
                enabled: user.enabled,
                emailVerified: user.emailVerified,
                familyName: user.firstName,
                givenName: user.lastName
            }
        }
    }
    /**
     * Converts a generic user to a keycloak user presentation
     * @param {GenericUser|*} user 
     */
    convertFrom(user) {
        // convert user to keycloak user
        const result = {
            id: user.id,
            username: user.name,
            email: user.alternateName,
            enabled: user.enabled,
            emailVerified: Object.prototype.hasOwnProperty.call(user, 'emailVerified') ? user.emailVerified : true,
            firstName: user.givenName,
            lastName: user.familyName
         };
         if (user.userCredentials && user.userCredentials.userPassword) {
             let algorithm;
             let password;
             if (/^\{md5}/ig.test(user.userCredentials.userPassword)) {
                throw new Error('Unsupported hash algorithm');
             } else if (/^\{clear}/.test(user.userCredentials.userPassword)) {
                password = user.userCredentials.userPassword.replace(/^\{clear}/ig, '');
            } else if (/^\{sha1}/.test(user.userCredentials.userPassword)) {
                throw new Error('Unsupported hash algorithm');
            } else {
                password = user.userCredentials.userPassword;
            }
             // noinspection JSUnusedAssignment
             Object.assign(result, {
                credentials: [
                    {
                        algorithm: algorithm,
                        temporary: Object.prototype.hasOwnProperty.call(user.userCredentials, 'temporary') ? user.userCredentials.temporary : false,
                        type: 'password',
                        value: password
                    }
                ]
             });
             // try to find credentials of type password that are temporary
             // note: keep this generic for other credential implementations in the feature
             const temporaryPassword = result.credentials.find((credential) => credential.type === 'password' && credential.temporary);
             if (temporaryPassword) {
                // assign UPDATE_PASSWORD action, so the user changes password after the first login
                 Object.assign(result, {
                    requiredActions: [
                        'UPDATE_PASSWORD'
                    ]
                 });
             }
         }
         return result;
    }


    /**
     * Creates a new user
     * @param {KeycloakUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     createKeycloakUser(user, options) {
        return new Promise((resolve, reject)=> {
            new Request('POST', new URL(`users`, this.settings.admin_uri))
                .set('Authorization', `Bearer ${options.access_token}`)
                .send(user)
                .end(responseHandler(resolve, reject));
        });
    }

    /**
     * Creates a new user
     * @param {GenericUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     async createUser(user, options) {
        // convert generic user
        const newUser = this.convertFrom(user);
        // create keycloak user
        await this.createKeycloakUser(newUser, options);
        // finally convert new user to generic user
        return newUser;
    }


    /**
     * Deletes a user
     * @param {{id: any}} user 
     * @param {KeycloakMethodOptions} options 
     */
     deleteUser(user, options) {
        return new Promise((resolve, reject)=> {
            if (user.id == null) {
                return reject(new DataError('E_IDENTIFIER', 'User may not be empty at this context.', null, 'User', 'id'));
            }
            new Request('DELETE', new URL(`users/${user.id}`, this.settings.admin_uri))
                .set('Authorization', `Bearer ${options.access_token}`)
                .end(responseHandler(resolve, reject));
        });
    }

}

module.exports = {
    KeycloakClientService
};
