### Keycloak Client Service for Universis Api Server

@universis/keycloak allows [Universis Api Server](https://gitlab.com/universis/universis) to use [Keycloak](https://www.keycloak.org/)
as backend authorization server.

#### Usage

Install @universis/keycloak as dependency of Universis Api Server:

    npm i @universis/keycloak

#### Service Configuration

Use KeycloakClientService as service strategy of OAuth2ClientService of [Universis Api Server](https://gitlab.com/universis/universis)

server/config/app.production.json

```json
{
  "services": [
    {
      "serviceType": "./services/oauth2-client-service#OAuth2ClientService",
      "strategyType": "@universis/keycloak#KeycloakClientService"
    }
  ]
}
```

Note: Check [@themost/express](https://github.com/kbarbounakis/most-data-express) version and update to ^1.3.5.

    npm update @themost/express

#### Application Configuration

##### server_uri

Configure Universis Api Server to use Keycloak endpoints to validate user access:

```json
{
  "settings": {
    "auth": {
      "server_uri": "https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/",
      "client_id": "universis-client",
      "client_secret": "secret"
    }
  }
}
```

##### issuer_uri

The `issuer_uri` attribute is the base URL of the Keycloak server.

```json
{
    "settings": {
      "auth": {
        "issuer_uri": "https://<keycloak server>/auth/realms/<keycloak realm>/",
        "client_id": "universis-client",
        "client_secret": "secret"
      }
    }
}
```
This setting will be used to validate and get well known configuration of the Keycloak server
and identify the endpoints that are going to be used by `KeycloakClientService`.
The usage of `server_uri` is optional and may be omitted if `issuer_uri` is defined.

##### client_id

The `client_id` attribute is the client identifier of the Universis Api Server.

##### client_secret

The `client_secret` attribute is the client secret of the Universis Api Server.
You should enable confidential access type for a keycloak client to get a client secret.

##### admin_uri

The `admin_uri` attribute is the base URL of the Keycloak server REST API.
If `admin_uri` is not defined, the `issuer_uri` will be used to construct the admin URI.

```json
{
  "settings": {
    "auth": {
      "issuer_uri": "https://<keycloak server>/auth/realms/<keycloak realm>/",
      "client_id": "universis-client",
      "client_secret": "secret",
      "admin_uri": "https://<keycloak server>/auth/admin/realms/<keycloak realm>/"
    }
  }
}
```


#### User management

`KeycloakClientService` offers operations for managing users. If you want to use keycloak api inside your application extend application configuration:

```json
{
  "settings": {
    "auth": {
      "server_uri": "https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/",
      "client_id": "universis-client",
      "client_secret": "secret",
      "admin_uri": "https://<keycloak server>/auth/admin/realms/<keycloak realm>/",
      "adminAccount": {
        "client_id": "admin-cli",
        "grant_type": "password",
        "username": "<a user who has query-users and manage-users roles>",
        "password": "<user password>"
      }
    }
  }
}
```

#### Client Application Configuration

Following Universis Api Server configuration update, configure each client application respectively:

e.g. change settings#auth section of universis-students configuration

    "auth": {
        "authorizeURL":"https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/auth",
        "logoutURL":"https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/logout?redirect_uri=http://localhost:7001/#/auth/login",
        "userProfileURL":"https://<api server>/api/users/me",
        "oauth2": {
            "clientID": "universis-client",
            "callbackURL": "http://localhost:7001/auth/callback/index.html",
            "scope": [
                "students"
            ]
        }
    }

#### Token introspection

Token introspection is the process of sending a token to the authorization server to determine
the active state of the token and to find out other information such as token type scope etc.

`@universis/keycloak^3.0.0` implements 2 different mechanisms for token introspection:

1. `HttpTokenIntrospectionStrategy` which uses HTTP POST request to introspect a token.
2. `JwtTokenIntrospectionStrategy` which uses JWT token introspection mechanism.

Both strategies are enabled by registering them at `services` section of application configuration.
The `HttpTokenIntrospectionStrategy` is used by default.

```json
{
  "services": [
    {
      "serviceType": "@universis/keycloak#KeycloakTokenIntrospectionStrategy",
      "strategyType": "@universis/keycloak#HttpTokenIntrospectionStrategy"
    }
  ]
}
```

or

```json
{
  "services": [
    {
      "serviceType": "@universis/keycloak#KeycloakTokenIntrospectionStrategy",
      "strategyType": "@universis/keycloak#JwtTokenIntrospectionStrategy"
    }
  ]
}
```

`HttpTokenIntrospectionStrategy` may use caching while introspecting a token against Keycloak server.
Use `settings/auth/introspectionLifespan` attribute which defines the absolute expiration timeout in seconds.

```json
{
  "settings": {
    "auth": {
      "introspectionLifespan": 120
    }
  }
}
```
