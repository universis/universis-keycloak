const {AbstractMethodError} = require('@themost/common');
const {OpenIDConnectService} = require('./openid-connect-service');

class UnknownTokenIssuerError extends Error {
    constructor() {
        super('Unknown token issuer');
        this.name = 'UnknownTokenIssuerError';
    }
}

class InvalidTokenError extends Error {
    constructor() {
        super('Invalid token');
        this.name = 'InvalidTokenError';
    }
}

class InvalidIssuerKeyError extends Error {
    constructor() {
        super('Invalid issuer key');
        this.name = 'InvalidIssuerKeyError';
    }
}

/**
 * @abstract
 * Token introspection strategy
 */
class KeycloakTokenIntrospectionStrategy extends OpenIDConnectService {
    constructor(app) {
        super(app);
    }

    // noinspection JSUnusedLocalSymbols
    /**
     * Introspect token
     * @param {string} token
     * @returns {Promise<any>}
     */
    // eslint-disable-next-line no-unused-vars
    introspect(token) {
        throw new AbstractMethodError('KeycloakTokenIntrospectionStrategy.introspect(token) method must be implemented.');
    }
}

module.exports = {
    UnknownTokenIssuerError,
    InvalidTokenError,
    InvalidIssuerKeyError,
    KeycloakTokenIntrospectionStrategy
}
