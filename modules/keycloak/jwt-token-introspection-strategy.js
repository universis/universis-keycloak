const {KeycloakTokenIntrospectionStrategy,
    InvalidTokenError,
    UnknownTokenIssuerError,
    InvalidIssuerKeyError
} = require('./keycloak-introspection-strategy')
const { Request } = require('superagent');
const {Observable} = require('rxjs');
const { switchMap } = require('rxjs/operators');
const {shareReplay} = require('rxjs/operators');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const { responseHandler } = require('./response-callback');

class JwtTokenIntrospectionStrategy extends KeycloakTokenIntrospectionStrategy {

    constructor(app) {
        super(app);
        // noinspection JSCheckFunctionSignatures
        this.keys = this.wellKnownConfiguration.pipe(switchMap(({ jwks_uri }) => {
            if (jwks_uri == null) {
                throw new Error('JWKS URI is not defined or is inaccessible.');
            }
            return new Observable((observer) => {
                void new Promise((resolve, reject) => {
                    return new Request('GET', jwks_uri)
                        .set('Accept', 'application/json')
                        .end(responseHandler(resolve, reject));
                }).then((result) => {
                    const keys = result.keys || [];
                    // set public key for future use
                    keys.filter(
                        (key) => Object.prototype.hasOwnProperty.call(key, 'x5c')
                    ).forEach((key) => {
                        // noinspection JSUnresolvedReference
                        const [cert] = key.x5c;
                        const pem = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----`;
                        // get public key
                        key.x5p = crypto.createPublicKey(pem).export({
                            type: 'spki',
                            format: 'pem'
                        });
                    });
                    observer.next(keys);
                    observer.complete();
                }).catch((err) => {
                    observer.error(err);
                });
            })
        }), shareReplay(1));
    }

    /**
     * @param token
     * @returns {Promise<*>}
     */
    introspect(token) {
        return new Promise((resolve, reject) => {
            void this.keys.subscribe((keys) => {
                try {
                    let decoded = jwt.decode(token, { complete: true });
                    if (decoded == null) {
                        return reject(new InvalidTokenError());
                    }
                    // get
                    const kid = decoded.header.kid;
                    // try to find key
                    // noinspection JSUnresolvedReference
                    const key = keys.find((k) => k.kid === kid);
                    if (key == null) {
                        return reject(new UnknownTokenIssuerError());
                    }
                    // noinspection JSUnresolvedReference
                    if (key.x5p == null) {
                        // throw error because public key is not defined
                        return reject(new InvalidIssuerKeyError());
                    }
                    // verify token
                    // noinspection JSCheckFunctionSignatures
                    void jwt.verify(token, key.x5p, {
                        algorithms: [
                            decoded.header.alg
                        ]
                    }, (err, decoded) => {
                        if (err) {
                            if (err.name === 'TokenExpiredError') {
                                return resolve({
                                    active: false
                                });
                            }
                            return reject(err);
                        }
                        // noinspection JSUnresolvedReference
                        const username = decoded.preferred_username;
                        const active = true;
                        return resolve(Object.assign(decoded, {
                            username,
                            active
                        }));
                    });
                } catch (err) {
                    reject(err);
                }
            }, (err) => {
                reject(err);
            });
        });
    }
}

module.exports = {
    JwtTokenIntrospectionStrategy
}
