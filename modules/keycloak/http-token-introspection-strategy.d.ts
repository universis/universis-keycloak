import {
    KeycloakTokenIntrospectionStrategy
} from './keycloak-introspection-strategy';
import {OpenIDConnectConfiguration} from './openid-connect-service';

export declare interface OpenIDConnectConfigurationWithIntrospection extends OpenIDConnectConfiguration {
    introspectionLifespan?: number;
}

export declare class HttpTokenIntrospectionStrategy extends KeycloakTokenIntrospectionStrategy {
    get settings(): OpenIDConnectConfigurationWithIntrospection;
}
