const {HttpError} = require('@themost/common');

function responseCallback(resolve, reject) {
    return function (err, response) {
        if (err) {
            /**
             * @type {import('superagent').Response}
             */
            const response = err.response
            if (response && response.headers['content-type'] === 'application/json') {
                // get body
                const clientError = response.body;
                const error = new HttpError(response.status);
                return reject(Object.assign(error, {
                    clientError
                }));
            }
            return reject(err);
        }
        if (response.status === 204 && response.headers['content-type'] === 'application/json') {
            return resolve(null);
        }
        return resolve(response.body);
    };
}

module.exports = {
    responseHandler: responseCallback
}
