import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataContext } from '@themost/data';
import {Observable} from 'rxjs';
import {OpenIDConnectService} from './openid-connect-service';

export declare interface KeycloakMethodOptions {
    access_token: string;
}

export declare interface KeycloakAuthorizeUser {
    client_id?: string;
    client_secret?: string;
    username: string;
    password: string;
    grant_type: string;
    scope?: string;
}

export declare interface KeycloakUserProfile {
    sub: string;
    name: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    email: string;
}

export declare interface GenericUser {
     id?: any; 
     additionalType?: string; 
     alternateName?: string; 
     description?: string; 
     givenName?: string; 
     familyName?: string; 
     image?: string; 
     name?: string; 
     url?: string; 
     dateCreated?: Date; 
     dateModified?: Date; 
     createdBy?: any; 
     modifiedBy?: any; 
     lockoutTime?: Date; 
     logonCount?: number; 
     enabled?: boolean; 
     lastLogon?: Date; 
     userCredentials?: {
         userPassword?: string;
         userActivated?: boolean;
         temporary?: boolean;
     }
}

export declare interface KeycloakUser {
    id?: any; 
    username?: string; 
    email?: string; 
    enabled?: boolean;
    emailVerified?: boolean;
    firstName?: string; 
    lastName?: string; 
    credentials?: {
        algorithm?: string,
        temporary?: boolean,
        type?: string,
        value?: string
    }
}


export declare class KeycloakClientService extends OpenIDConnectService {
    constructor(app: ApplicationBase)
    getProfile(context: DataContext, token: string): Promise<KeycloakUserProfile>;
    getTokenInfo(context: DataContext, token: string): Promise<any>;
    getContextTokenInfo(context: DataContext): Promise<any>;
    authorize(authorizeUser: KeycloakAuthorizeUser): Promise<{ access_token?: string, refresh_token?: string, expires_in?: number}>;
    getUser(username: string, options: KeycloakMethodOptions): Promise<any>;
    getUserById(user_id: any, options: KeycloakMethodOptions): Promise<any>;
    getUserByEmail(email: string, options: KeycloakMethodOptions): Promise<any>;
    updateUser(user: GenericUser | any, options: KeycloakMethodOptions): Promise<any>;
    createUser(user: GenericUser | any, options: KeycloakMethodOptions): Promise<any>;
    deleteUser(user: { id: any }, options: KeycloakMethodOptions): Promise<any>;
}
