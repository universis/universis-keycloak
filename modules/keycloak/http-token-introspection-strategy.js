const {TextUtils} = require('@themost/common');
const {Request} = require('superagent');
const {KeycloakTokenIntrospectionStrategy} = require('./keycloak-introspection-strategy')
const { responseHandler } = require('./response-callback');

class HttpTokenIntrospectionStrategy extends KeycloakTokenIntrospectionStrategy {

    constructor(app) {
        super(app);
        // get introspection lifespan
        const introspectionLifespan = this.settings.introspectionLifespan || 0;
        /**
         * @type {import('@themost/data').DataCacheStrategy}
         */
        this.cache = null;
        if (introspectionLifespan > 0) {
            // get cache service
            this.cache = app.getConfiguration().getStrategy(function DataCacheStrategy() {});
            // set expiration timeout
            this.introspectionLifespan = introspectionLifespan;
        }
    }

    /**
     * @param token
     * @returns {Promise<*>}
     */
    introspect(token) {
        if (this.cache == null) {
            return this.introspectInt(token);
        }
        // get token hash
        const str = TextUtils.toMD5(token);
        // get from cache or call token introspection
        // noinspection JSCheckFunctionSignatures
        return this.cache.getOrDefault(`/token/info#${str}`, () => {
            return this.introspectInt(token);
        }, this.introspectionLifespan);
    }

    /**
     * @param token
     * @returns {Promise<*>}
     */
    introspectInt(token) {
        return new Promise((resolve, reject)=> {
            void this.wellKnownConfiguration.subscribe((wellKnownConfiguration) => {
                const { introspection_endpoint } = wellKnownConfiguration;
                if (introspection_endpoint == null) {
                    return reject(new Error('Introspection endpoint is not defined or is inaccessible.'));
                }
                new Request('POST', introspection_endpoint)
                    .auth(this.settings.client_id, this.settings.client_secret)
                    .type('form')
                    .send({
                        "token_type_hint":"access_token",
                        "token":token,
                    })
                    .end(responseHandler(resolve, reject));
            }, (err) => {
                return reject(err);
            });
        });
    }
}

module.exports = {
    HttpTokenIntrospectionStrategy
}
