import {ApplicationService} from '@themost/common';

export declare interface KeycloakTokenIntrospectionResponse {
    exp: number
    iat: number
    jti: string
    iss: string
    aud: string
    sub: string
    typ: string
    azp: string
    session_state?: string
    name: string
    given_name: string
    family_name: string
    preferred_username: string
    email: string
    email_verified: boolean
    acr?: string
    scope: string
    sid: string
    client_id?: string
    username: string
    active: boolean
}

export declare class UnknownTokenIssuerError extends Error {}
export declare class InvalidTokenError extends Error {}
export declare class InvalidIssuerKeyError extends Error {}

export declare class KeycloakTokenIntrospectionStrategy extends ApplicationService {
    introspect(token: string): Promise<KeycloakTokenIntrospectionResponse>;
}
