import {
    KeycloakTokenIntrospectionStrategy
} from './keycloak-introspection-strategy';
import {Observable} from 'rxjs';

export declare class JwtTokenIntrospectionStrategy extends KeycloakTokenIntrospectionStrategy {
    keys: Observable<Array<any>>;
}
