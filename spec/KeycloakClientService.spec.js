import {DataApplication, DataCacheStrategy} from '@themost/data';
import { Guid } from '@themost/common';
import { KeycloakClientService, JwtTokenIntrospectionStrategy } from '../modules/keycloak';
import path from 'path';
import { Request } from 'superagent';
import { first } from 'rxjs/operators';
import jwt from 'jsonwebtoken';
import fs from 'fs'

/**
 * @returns {Promise<string>}
 */
function createUnknownAccessToken() {
    return new Promise((resolve, reject) => {
        try {
            const privateKey = fs.readFileSync(path.resolve(__dirname, 'key.pem'), 'utf8');
            const iat = Math.round(new Date().getTime() / 1000);
            const exp = iat + 300 ;
            const access_token = jwt.sign({
                "exp": exp,
                "iat": iat,
                "jti": Guid.newGuid().toString(),
                "iss": "http://localhost:8080/realms/master",
                "sub":  Guid.newGuid().toString(),
                "typ": "Bearer",
                "azp": "admin-cli",
                "session_state":  Guid.newGuid().toString(),
                "acr": "1",
                "scope": "profile email",
                "sid":  Guid.newGuid().toString(),
                "email_verified": true,
                "preferred_username": "admin",
                "given_name": "",
                "family_name": "",
                "email": "admin1@example.com"
            }, privateKey, {
                algorithm: 'RS256'
            });
            return resolve(access_token);
        } catch (err) {
            return reject(err);
        }
    });
}

/**
 * @returns {Promise<string>}
 */
function createExpiredAccessToken() {
    return new Promise((resolve, reject) => {
        try {
            const privateKey = fs.readFileSync(path.resolve(__dirname, 'key.pem'), 'utf8');
            const iat = Math.round(new Date().getTime() / 1000) - 3600;
            const exp = iat + 300 ;
            const access_token = jwt.sign({
                "exp": exp,
                "iat": iat,
                "jti": Guid.newGuid().toString(),
                "iss": "http://localhost:8080/realms/master",
                "sub":  Guid.newGuid().toString(),
                "typ": "Bearer",
                "azp": "admin-cli",
                "session_state":  Guid.newGuid().toString(),
                "acr": "1",
                "scope": "profile email",
                "sid":  Guid.newGuid().toString(),
                "email_verified": true,
                "preferred_username": "admin",
                "given_name": "",
                "family_name": "",
                "email": "admin1@example.com"
            }, privateKey, {
                algorithm: 'RS256'
            });
            return resolve(access_token);
        } catch (err) {
            return reject(err);
        }
    });
}

describe('KeycloakClientService', () => {
    /**
     * @type {DataApplication}
     */
    let app;
    beforeAll(() => {
        app = new DataApplication(path.resolve(__dirname));
        // noinspection SpellCheckingInspection
        app.configuration.setSourceAt("settings/auth", {
            "unattendedExecutionAccount": "2S4TgUW6==",
            // "issuer_uri": "http://localhost:8080/realms/master/",
            "server_uri": "http://localhost:8080/realms/master/protocol/openid-connect",
            // "admin_uri": "http://localhost:8080/admin/realms/master/",
            "client_id": "universis-client",
            "client_secret": "vPZksaC5AgkHV8304HLUkwdG5X6oY75F",
            "adminAccount": {
                "username": "admin",
                "password": "secret"
            }
        });
    });

    afterAll(async () => {
        /**
         * @type {DataCacheStrategy|*}
         */
        const service = app.configuration.getStrategy(DataCacheStrategy);
        if (typeof service.finalize === 'function') {
            await service.finalize();
        }
    })

    it('should create service', () => {
        const service = new KeycloakClientService(app);
        expect(service).toBeTruthy();
    });
    it('should validate issuer URI', () => {
        const service = new KeycloakClientService(app);
        expect(service.settings.issuer_uri).toEqual('http://localhost:8080/realms/master/');
    });
    it('should validate admin URI', () => {
        const service = new KeycloakClientService(app);
        expect(service.settings.admin_uri).toEqual('http://localhost:8080/admin/realms/master/');
    });
    it('should should authorize user', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        expect(token).toBeTruthy();
        expect(token.access_token).toBeTruthy();
        expect(token.expires_in).toBeTruthy();
        await expect(service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret1'
        })).rejects.toBeTruthy();
    });

    it('should verify token', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        expect(token).toBeTruthy();
        expect(token.access_token).toBeTruthy();
        const introspection = new JwtTokenIntrospectionStrategy(app);
        let info = await introspection.introspect(token.access_token);
        expect(info).toBeTruthy();
        info = await introspection.introspect(token.access_token);
        expect(info).toBeTruthy();
        expect(info.active).toBeTruthy();
        expect(info.username).toEqual('admin');
    });

    it('should get token info', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        expect(token).toBeTruthy();
        expect(token.access_token).toBeTruthy();
        const info = await service.getTokenInfo(null, token.access_token);
        expect(info).toBeTruthy();
        expect(info.active).toBeTruthy()

    });

    it('should throw error for expired token', async () => {
        const service = new KeycloakClientService(app);
        // this limited client has been configured to provide an access token which expires immediately
        const token = await service.authorize({
            client_id: 'limited-client',
            client_secret: 'ZICvLfRhNz1hbn8FXyUwUub9Qptjjpnh',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        expect(token).toBeTruthy();
        const { access_token } = token;
        const introspection = new JwtTokenIntrospectionStrategy(app);
        const info = await introspection.introspect(access_token);
        expect(info).toBeTruthy();
        expect(info.active).toBeFalsy();
    });

    it('should throw error for unknown token', async () => {
        const service = new KeycloakClientService(app);
        expect(service).toBeTruthy();
        const introspection = new JwtTokenIntrospectionStrategy(app);
        const access_token = await createUnknownAccessToken();
        await expect(introspection.introspect(access_token)).rejects.toThrow('Unknown token issuer')
    });

    it('should throw error for invalid token', async () => {
        const service = new KeycloakClientService(app);
        const introspection = new JwtTokenIntrospectionStrategy(app);
        expect(service).toBeTruthy();
        await expect(introspection.introspect('eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJuWmNDUTlUQ1hHcEtpY3MwMnFmdlljU2FxMk9IYW5RMGZaYXE1R0RhQlI0In0')).rejects.toThrow('Invalid token')
    });

    it('should should authorize admin account', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: service.settings.adminAccount.username,
            password: service.settings.adminAccount.password
        });
        expect(token).toBeTruthy();
    });

    it('should should get user by name', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        const user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        expect(user.name).toBe('admin');
    });

    it('should should get user by id', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        let user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        user = await service.getUserById(user.id, {
            access_token: token.access_token
        });
        expect(user.name).toBe('admin');
    });

    it('should should get user by email', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        const user = await service.getUserByEmail('test@example.com', {
            access_token: token.access_token
        });
        expect(user).toBeFalsy();
    });

    it('should should update user', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        let user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        await service.updateUser({
            id: user.id,
            alternateName: 'admin1@example.com',
            emailVerified: true
        }, {
            access_token: token.access_token
        });
        user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user.alternateName).toBe('admin1@example.com');
    });

    it('should should create user', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        let user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        await service.createUser({
            name: 'alexis.rees@example.com',
            alternateName: 'alexis.rees@example.com',
            enabled: true,
            emailVerified: true,
            firstName: 'Alexis',
            lastName: 'Rees',
            userCredentials: {
                userPassword: `{clear}testSecret`
            }
        }, {
            access_token: token.access_token
        });
        user = await service.getUser('alexis.rees@example.com', {
            access_token: token.access_token
        });
        expect(user.name).toBe('alexis.rees@example.com');

        //try to log in
        const userToken = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'alexis.rees@example.com',
            password: 'testSecret'
        });
        expect(userToken).toBeTruthy();
        expect(userToken.access_token).toBeTruthy();

        await service.deleteUser({
            id: user.id
        }, {
            access_token: token.access_token
        });
        user = await service.getUser('alexis.rees@example.com', {
            access_token: token.access_token
        });
        expect(user).toBeFalsy();

    });

    it('should should get well known configuration', async () => {
        const service = new KeycloakClientService(app);
        const spy = jest.spyOn(Request.prototype, 'end', undefined);
        let config = await service.wellKnownConfiguration.pipe(first()).toPromise();
        expect(config).toBeTruthy();
        // try to call this method again
        config = await service.wellKnownConfiguration.pipe(first()).toPromise();
        expect(config).toBeTruthy();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should should get keycloak server keys', async () => {
        const service = new KeycloakClientService(app);
        expect(service).toBeTruthy();
        const introspection = new JwtTokenIntrospectionStrategy(app);
        let keys = await introspection.keys.pipe(first()).toPromise();
        expect(keys).toBeTruthy();
    });

});
